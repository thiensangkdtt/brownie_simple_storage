from brownie import SimpleStorage, accounts, network, config


def test_deploy():
    # Arrange
    account = get_account()
    # Accounts
    simple_storage = SimpleStorage.deploy({"from": account})
    starting_value = simple_storage.retrieve()
    expected = 0
    # Assert
    assert starting_value == expected


def get_account():
    if network.show_active() == "development":
        return accounts[0]
    else:
        accounts.add(config["wallets"]["from_key"])


def test_updating_storage():
    # Arrange
    account = accounts[0]
    simple_storage = SimpleStorage.deploy({"from": account})
    # Accounts
    expected = 15
    transaction = simple_storage.store(expected, {"from": account})
    transaction.wait(1)
    # Assert
    assert expected == simple_storage.retrieve()
